class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors.add attribute, (options[:message] || "is not an email")
    end
  end
end


class Order < ApplicationRecord
  validates :sender_full_name, presence: true
  validates :sender_address, presence: true
  validates :sender_email, presence: true, email: true
  validates :sender_phone_no, length: { is: 10}
  validates :sender_pin_code, length: { is: 6}
  validates :receiver_full_name, presence: true
  validates :receiver_address, presence: true
  validates :receiver_email, presence: true, email: true
  validates :receiver_phone_no, length: { is: 10}
  validates :receiver_pin_code, length: { is: 6}
  validates :weight, numericality: true
  validates :type_of_service, :inclusion=> { :in => ['Regular', 'Speed Post'] }
  validates :cost_of_service, numericality: true
  validates :payment, :inclusion=> { :in => ['COD', 'Prepaid'] }
  validates :status, :inclusion=> { :in => ['Sent', 'In Transit', 'Delivered'] }
end
