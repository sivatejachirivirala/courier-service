class EmailJob < ApplicationJob
  queue_as :default

  def perform(*args)
    OrderMailer.send_email(args[0]).deliver_now
  end
end
