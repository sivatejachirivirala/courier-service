class OrdersController < ApplicationController

  def create
    @order = Order.new(order_params)
    if @order.save
      EmailJob.perform_later @order
      redirect_to :new_order, notice: 'Order Created Successfully'
    else
      flash.now[:alert] = @order.errors.full_messages
      render :new
    end
  end

  def index
    @orders = Order.all
  end

  def edit_status
    @order = Order.find(params[:id])
  end

  def change_status
    @order = Order.find(params[:id])
    @order.status = params[:status]
    if @order.save
      redirect_to :orders
    else
      render :edit_status
    end
  end

  def track_order
    @order = Order.find(params[:id])
  end

  def status
    @status = Order.find(params[:id]).status
  end

  def search
  end

   def new
     @order = Order.new
   end

  private

  def order_params
    params.require(:order).permit(:sender_full_name, :sender_address, :sender_email, :sender_phone_no, :sender_pin_code, :receiver_full_name, :receiver_address, :receiver_email, :receiver_phone_no, :receiver_pin_code, :weight, :type_of_service, :payment).merge(:cost_of_service => cost, :status => 'Sent')
  end

  def cost
    return params[:order][:weight].to_f * 10 if params[:order][:type_of_service] == 'Regular'
    params[:order][:weight].to_f * 20
  end
end
