class OrderMailer < ApplicationMailer
  default from: 'delivery@gmail.com'

  def send_email *args
    @order = args[0]
    mail_to(to: @order.sender_email, cc: @order.reciever_email, subject: 'Order Created Successfully' )
  end
end
