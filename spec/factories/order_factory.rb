FactoryBot.define do
  
  factory :order do
   sender_full_name { "test sender" }
   sender_address { "test sender address" }
   sender_email { "test@test.com" }
   sender_phone_no { "0912345678" }
   sender_pin_code { "456789" }
   receiver_full_name { "test receiver" }
   receiver_address { "test receiver address" }
   receiver_email { "test@example.com" }
   receiver_phone_no { "1234567880" }
   receiver_pin_code { "522004" }
   weight { 12.0 }
   type_of_service { "Regular" }
   cost_of_service { 120.0 }
   payment { "COD" }
   status { "Sent" }
  end
end