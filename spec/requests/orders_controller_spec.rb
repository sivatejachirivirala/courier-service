require 'rails_helper'

RSpec.describe "OrdersControllers", type: :request do
  let(:order) {FactoryBot.create :order}

  describe "GET /orders" do
    it 'renders all orders' do
      get "/orders"
      expect(response).to render_template(:index)
    end
  end

  describe "POST /orders" do
  	let(:params) { { sender_full_name: 'test sender',
                   sender_address: 'test sender address',
                   sender_email: 'test@test.com',
                   sender_phone_no: '0912345678',
                   sender_pin_code: '456789',
                   receiver_full_name: 'test receiver',
                   receiver_address: 'test receiver address',
                   receiver_email: 'test@example.com',
                   receiver_phone_no: '1234567880',
                   receiver_pin_code: '522004',
                   weight: 12.0,
                   type_of_service: 'Regular',
                   payment: 'COD' } }

    it 'does not create a order' do
      post '/orders', params: { order: { sender_full_name: "test sender" } }
      expect(response).to render_template(:new)
      expect(response.body).to include("test sender")
    end

    it 'creates a order' do
      post '/orders', params: { order: params }
      expect(response).to redirect_to(:new_order )
    end
  end

  describe "GET /orders/new" do
    it 'renders new order template' do
      get "/orders/new"
      expect(response).to render_template(:new)
      expect(response.body).to include("New Order")
    end
  end

  describe "GET /status" do
    it 'renders status template' do
      get "/status", :params => { id: order.id }
      expect(response).to render_template(:status)
      expect(response.body).to include(" Your courier has been\n  Sent\n")
    end
  end

  describe "GET /track/:id" do
    it 'renders track_order template' do
      get "/track/#{order.id}"
      expect(response).to render_template(:track_order)
      expect(response.body).to include("test sender")
    end
  end

  describe "GET /edit_status" do
    it 'renders edit_status template' do
      get "/edit_status/#{order.id}"
      expect(response).to render_template(:edit_status)
    end
  end

  describe "POST /order" do
    it 'does not update the order status' do
      post '/order', :params => { id: order.id, status: 'In Transt' }
      expect(response).to render_template(:edit_status)
    end

    it 'updates the order status' do
      post '/order', :params => { id: order.id, status: 'In Transit' }
      expect(response).to redirect_to(:orders)
    end
  end

  describe "GET /search" do
    it 'it renders search template' do
      get "/search"
      expect(response).to render_template(:search)
      expect(response.body).to include("Search")
    end
  end
end