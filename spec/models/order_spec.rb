require 'rails_helper'

RSpec.describe Order, type: :model do
  subject {
    described_class.new(sender_full_name: 'test sender',
                        sender_address: 'test sender address',
                        sender_email: 'test@test.com',
                        sender_phone_no: '0912345678',
                        sender_pin_code: '456789',
                        receiver_full_name: 'test receiver',
                        receiver_address: 'test receiver address',
                        receiver_email: 'test@example.com',
                        receiver_phone_no: '1234567880',
                        receiver_pin_code: '522004',
                        weight: 12.0,
                        type_of_service: 'Regular',
                        cost_of_service: 120.0,
                        payment: 'COD',
                        status: 'Sent')
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without sender_full_name" do
    subject.sender_full_name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without sender_address" do
    subject.sender_address = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a sender_email" do
    subject.sender_email = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a sender_phone_no" do
    subject.sender_phone_no = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a sender_pin_code" do
    subject.sender_pin_code = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a receiver_full_name" do
    subject.receiver_full_name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a receiver_address" do
    subject.receiver_address = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a receiver_email" do
    subject.receiver_email = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a receiver_phone_no" do
    subject.receiver_phone_no = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a receiver_pin_code" do
    subject.receiver_pin_code = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a weight" do
    subject.weight = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a type_of_service" do
    subject.type_of_service = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a cost_of_service" do
    subject.cost_of_service = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a payment" do
    subject.payment = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a status" do
    subject.status = nil
    expect(subject).to_not be_valid
  end  
end
