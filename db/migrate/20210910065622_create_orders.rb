class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :sender_full_name
      t.text :sender_address
      t.string :sender_email
      t.string :sender_phone_no
      t.string :sender_pin_code
      t.string :receiver_full_name
      t.text :receiver_address
      t.string :receiver_email
      t.string :receiver_phone_no
      t.string :receiver_pin_code
      t.float :weight
      t.string :type_of_service
      t.float :cost_of_service
      t.string :payment
      t.string :status
      t.string :uuid

      t.timestamps
    end
  end
end
