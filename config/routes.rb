Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :orders, only: [:create, :new, :index]
  get "/status", to: "orders#status"
  get "/track/:id", to: "orders#track_order"
  get "/edit_status/:id", to: "orders#edit_status", as: "edit_status"
  post "/order", to: "orders#change_status"
  get "/search", to: "orders#search"
end
